ARG DVER=3.18

FROM registry.gitlab.com/a16bitsysop/alpine-dev-local/main:$DVER

WORKDIR /usr/local/bin

COPY --chmod=755 build-aport.sh ./

CMD [ "build-aport.sh" ]
